var express = require('express');
var router = express.Router();

// Company model
const Companies = require('../models/Companies');

// vessel models
const Vessles = require('../models/Vessels');

// position model
const Positions = require('../models/Positions');

/* GET users listing. */
router.get('/', function(req, res, next) {
	res.send('respond with a resource');
});

router.post('/vessles/map', function(req, res) {
	const { callsign, name, imo, mmsi, company } = req.body;
	let errors = [];

	// Check required fields
	if (!callsign || !name || !imo || !mmsi || !company) {
		errors.push({ msg: 'No data received' });
	}
	if (
		Vessles.findOne({ mmsi: mmsi }).then((vessels) => {
			if (vessels) {
				// vessel exists
				const newVessel = new Vessles({
					callsign,
					name,
					imo,
					mmsi,
					company
				});
			}
		})
	);
});

router.post('/vessles/map', function(req, res) {
	const { date, callsign, at, sog, heading, course, loc } = req.body;
	let errors = [];

	// Check required fields
	if (!date || !callsign || !at || !sog || !heading || !course || !loc) {
		errors.push({ msg: 'No data received' });
	}
	if (
		Companies.findOne({ mmsi: mmsi }).then((comp) => {
			if (comp) {
				// vessel exists
				const newVessel = new Vessles({
					date,
					callsign,
					at,
					sog,
					heading,
					course,
					course,
					loc
				});
			}
		})
	);
});

router.post('/vessles/map', function(req, res) {
	const { number, name, icon, logo } = req.body;
	let errors = [];

	// Check required fields
	if (!number || !name || !icon || !logo) {
		errors.push({ msg: 'No data received' });
	}
	if (
		Positions.findOne({ mmsi: mmsi }).then((pos) => {
			if (pos) {
				// vessel exists
				const newVessel = new Vessles({
					number,
					name,
					icon,
					logo
				});
			}
		})
	);
});

module.exports = router;
