var express = require('express');
var router = express.Router();
var axios = require('axios');
const NodeCache = require('node-cache');
const myCache = new NodeCache();

let hitCount = 0;

/* GET home page. */
router.get('/', function(req, res, next) {
	res.render('index', { title: 'Express' });
});

const mmsiOfInterest = [
	'367029520',
	'366909730',
	'367128570',
	'366744010',
	'368098000',
	'367324810',
	'367437430',
	'367188386',
	'369010000',
	'366692000',
	'366796250',
	'367340160',
	'338025000',
	'538002695',
	'367525000',
	'366896390',
	'369118000',
	'369053000',
	'367157000',
	'367131000',
	'303589000',
	'367366670',
	'366942880',
	'338570000',
	'366053209',
	'366924720',
	'366806940',
	'367421990',
	'367785950',
	'367389840',
	'367087140',
	'367756560',
	'338107888',
	'338160398',
	'367310140',
	'367338310',
	'367189030',
	'367413270',
	'367473140',
	'367473180',
	'368683000',
	'367473150',
	'367652280',
	'367473230',
	'367466940',
	'367317850',
	'367141920',
	'368756000',
	'369970726',
	'366972000',
	'366971000',
	'338997000',
	'366982000',
	'369970571',
	'369970446',
	'507027',
	'367333540',
	'367500770',
	'367176730',
	'367323150',
	'367529860',
	'368954410',
	'366955430',
	'303390000',
	'369305000',
	'367500810',
	'367091750',
	'338111149',
	'367766220',
	'367453000'
];

const shipNamesOfInterest = [
	'MICHIGAN',
	'JP BOISSEAU',
	'DELAWARE BAY',
	'ATLANTIC SALVOR',
	'STUYVESANT',
	'PAULA LEE',
	'COLUMBIA',
	'OHIO',
	// 'ELLIS ISLAND', no
	'DREDGE TEXAS',
	'TERRAPIN ISLAND',
	'DREDGE ILLINOIS',
	'CAROLINA',
	'GL54',
	'SUGAR ISLAND',
	'PADRE ISLAND',
	'DREDGE 55',
	'DREDGE NEW YORK',
	'LIBERTY ISLAND',
	'DREDGE 51',
	'ALASKA',
	'DODGE ISLAND',
	// 'DREDGE 53', no
	'VULCAN',
	'NEWPORT',
	'BAYPORT',
	// 'WESTPORT', no
	'DREDGE NJORD',
	'HAGAR',
	'VALHALLA',
	// 'FRANK BECHTOLT', no
	'H.R. MORRIS',
	'ROBERT M. WHITE',
	'DREDGE EINAR',
	'GLENN EDWARDS',
	'PETER DEJONG',
	'DREDGE HAMPTON ROADS',
	'SAVANNAH DREDGE',
	'DREDGE E STROUD',
	'DREDGE 32',
	'MISSOURI H',
	'MIKE HOOKS',
	'VIRGINIAN',
	'ATLANTIC',
	'CHARLESTON',
	'PULLEN',
	// 'JEKYLL ISLAND', no
	// 'HAMPTON', no
	'DREDGE 428',
	'ESSEX',
	'LINDA LAQUAY',
	'JOHN C.LAQUAY',
	'WAYMON L BOYD',
	'HURLEY',
	'DREDGE GOETZ',
	'ESSAYONS',
	'YAQUINA',
	'MCFARLAND',
	'DREDGE WHEELER',
	'MURDEN',
	'CURRITUCK',
	'WEEKS 551',
	'BORINQUEN',
	'G D MORGRAN',
	'WEEKS 506',
	'E W ELLEFSEN',
	'C R MCCASKILL',
	'B.E. LINDHOLM',
	'R S WEEKS',
	'RN WEEKS',
	'W475  MAGDALEN',
	'CAPT FRANK',
	// 'W549', no
	// 'VENTURE', no
	'CAPT. AJ FOURNIER',
	'FJ BELESIMO',
	'DALE PYATT',
	// 'Wood1',  no
	'ATCHAFALAYA'
];

router.get('/hello', async function(req, res, next) {
	//
	const cashmanData = myCache.get('cashmanData');

	if (!cashmanData) {
		hitCount++;
		console.log(`hit ${hitCount} number of times`);
		const { data } = await axios.get(
			// 'http://data.aishub.net/ws.php?username=AH_3076_929F7762&format=1&output=json&compress=0&latmin=12.11&latmax=48.95&lonmin=-124.97&lonmax=-58.95'

			'http://data.aishub.net/ws.php?username=AH_3076_929F7762&format=1&output=json&compress=0'
		);

		console.log({ data });

		const [ metaData, ships ] = data;

		const shipsOfInterest = ships.filter(
			(ship) => mmsiOfInterest.includes(ship.MMSI) || shipNamesOfInterest.includes(ship.NAME)
		);

		myCache.set('cashmanData', shipsOfInterest, 70);

		res.send(data);

		return;
	}

	console.log('this is the data:', cashmanData);
	res.send(cashmanData);
});

module.exports = router;
